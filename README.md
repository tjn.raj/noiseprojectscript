# Noise Project Scripts

Scripts for Download (image & audio) and Upload (video)

## Getting Started

### Prerequisites

```
python3 installed
python library requests
```

### Installing

```
git clone https://gitlab.com/tjn.raj/noiseprojectscript.git
```

download.py
upload.py

## Running the tests

```python
python3 download.py id # id of noise from database => will download image and audio
python3 upload.py id videoFileName # id of noise from database, videfile => will post video on server
```

## Built With

python

## Authors

Narmaraj Thayanithy

