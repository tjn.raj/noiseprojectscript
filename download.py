# python3 download.py id

import os
from urllib.parse import urlparse
import requests
import json
import sys

id = sys.argv[-1]
urlApi = 'http://localhost:3000/api/noises/' + id
response = requests.get(urlApi)
imageName = json.loads(response.text)["photo"].split("/")[-1]
audioName = json.loads(response.text)["audio"].split("/")[-1]

urlImage = 'http://localhost:3000/images/' + imageName
urlAudio = 'http://localhost:3000/audios/' + audioName
myImage = requests.get(urlImage)
open(imageName, 'wb').write(myImage.content)

myAudio = requests.get(urlAudio)
open(audioName, 'wb').write(myAudio.content)
